package com.cpt202.team.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    
    //http://localhost:8080/
    @GetMapping("/")
    public String name(Model model){
        model.addAttribute("username", "Thomas");
        return "home";
    }
    
}
